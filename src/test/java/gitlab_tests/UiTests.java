package gitlab_tests;

import com.codeborne.selenide.Selenide;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("UI")
public class UiTests {
    String url = "https://www.petfinder.com/";

    @Test
    void checkOpenPageTest() {
        assertThat($x("//input[@id='simpleSearchAnimalType']").isDisplayed()).isEqualTo(true);
    }

    @Test
    void checkButtonsTest() {
        assertThat($$x("//li[starts-with(@class, 'QuizzleCard-module')]").size()).isEqualTo(4);
    }

    @BeforeEach
    public void setUp() {

        open(url);
    }

    @AfterEach
    public void tearDown() {
        Selenide.closeWebDriver();
    }
}
