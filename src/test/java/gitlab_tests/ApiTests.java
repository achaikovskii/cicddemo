package gitlab_tests;

import Components.Api.ResponseIpApi;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@Tag("Api")
public class ApiTests {
    String api = "https://ipinfo.io/%s/geo";
    String correctIp = "161.185.160.93";
    String incorrectIp = "161.185.160.";

    @Test
    void checkCorrectApiTest() {
        ResponseIpApi response = given().get(api.formatted(correctIp)).then().extract().body().as(ResponseIpApi.class);
        assertThat(response.getIp()).isEqualTo(correctIp);
    }

    @Test
    void checkIncorrectApiTest() {
        ResponseIpApi response = given().get(api.formatted(incorrectIp)).then().extract().body().as(ResponseIpApi.class);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(response.getStatus()).isEqualTo("404");
            softly.assertThat(response.getError().getTitle()).isEqualTo("Wrong ip");
            softly.assertThat(response.getError().getMessage()).isEqualTo("Please provide a valid IP address");
        });
    }
}
