package Components.Api;

import lombok.Data;

@Data
public class ResponseIpApi {
    private String ip;
    private String city;
    private String region;
    private String country;
    private String loc;
    private String org;
    private String postal;
    private String timezone;
    private String readme;
    private String status;
    private Errors error;

    @Data
    public static class Errors {
        private String title;
        private String message;
    }
}
